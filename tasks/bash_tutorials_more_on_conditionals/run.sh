#!/bin/bash

read a
read b
read c

if [[ $a == $b ]] && [[ $b == $c ]];
then
  echo -n "EQUILATERAL"
elif [[ $a == $b ]] || [[ $b == $c ]] || [[ $a == $c ]];
then
  echo -n "ISOSCELES"
else
  echo -n "SCALENE"
fi
