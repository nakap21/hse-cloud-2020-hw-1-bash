#!/bin/bash

i=0
while read country
do
    if [[ $country != *"a"* ]] && [[ $country != *"A"* ]]; then
      all_countries[i]=$country
      i=$((i + 1))
    fi
done

echo ${all_countries[@]}
