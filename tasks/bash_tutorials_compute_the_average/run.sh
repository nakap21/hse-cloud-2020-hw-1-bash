#!/bin/bash

read n

for ((i = 1; i <= $n; i++))
do
  read num
  sum=$(($sum + $num))
done

printf "%.3f\n" `echo "$sum/$n" | bc -l`
