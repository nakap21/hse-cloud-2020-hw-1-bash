#!/bin/bash

read a
read b

if (($a < $b)); then
    echo -n "X is less than Y";
elif (($a > $b)); then
    echo -n "X is greater than Y";
else
    echo -n "X is equal to Y";
fi
